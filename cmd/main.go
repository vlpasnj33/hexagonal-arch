package main

import (
	"flag"

	"github.com/gin-gonic/gin"
	"gitlab.com/vlpasnj33/hexagonal-arch/internal/adapters/handler"
	"gitlab.com/vlpasnj33/hexagonal-arch/internal/adapters/repository"
	"gitlab.com/vlpasnj33/hexagonal-arch/internal/core/services"
)

var (
	repo            = flag.String("db", "postgres", "Database for storing messages")
	// redisHost   = "localhost:6379"
	messageHandler  *handler.MessageHandler
	svc         	*services.MessengerService
)

func main() {
	flag.Parse()

	store := repository.NewMessengerPostgresRepository()
	svc = services.NewMessengerService(store)

	initRoutes()
}

func initRoutes() {
	router := gin.Default()

	handler := handler.NewMessageHandler(*svc)

	router.GET("/messages/:id", handler.ReadMessage)
	router.GET("/messages", handler.ReadMessages)
	router.POST("/messages", handler.SaveMessage)

	router.Run(":5000")
 }