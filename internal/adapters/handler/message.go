package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/vlpasnj33/hexagonal-arch/internal/core/domain"
	"gitlab.com/vlpasnj33/hexagonal-arch/internal/core/services"
)

type MessageHandler struct {
	svc services.MessengerService
}

func NewMessageHandler(s services.MessengerService) *MessageHandler {
	return &MessageHandler{svc: s}
}

func (h *MessageHandler) SaveMessage(ctx *gin.Context) {
	var message domain.Message 
	if err := ctx.ShouldBindJSON(&message); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"Error": err,
		}) 
		return
	}

	err := h.svc.SaveMessage(message)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"message": "New message created successfully",
	})	 
}

func (h *MessageHandler) ReadMessage(ctx *gin.Context) {
	id := ctx.Param("id")
	message, err := h.svc.ReadMessage(id)
 
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, message)
}

func (h *MessageHandler) ReadMessages(ctx *gin.Context) {
	messages, err := h.svc.ReadMessages()
 
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, messages)
}
