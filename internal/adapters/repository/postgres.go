package repository

import (
	"errors"
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/vlpasnj33/hexagonal-arch/internal/core/domain"
)

type MessengerPostgresRepository struct {
	db *gorm.DB
}

func NewMessengerPostgresRepository() *MessengerPostgresRepository {
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbname := os.Getenv("DB_NAME")

	conn := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
       host,
       port,
       user,
       dbname,
       password,
	)

	db, err := gorm.Open("postgres", conn)
	if err != nil {
		panic(err)
	}
	db.AutoMigrate(&domain.Message{})

	return &MessengerPostgresRepository{
		db: db,
	}
}

func (m *MessengerPostgresRepository) SaveMessage(message domain.Message) error {
	query := m.db.Create(&message)
	if query.RowsAffected == 0 {
		return errors.New(fmt.Sprintf("messages not saved: %v", query.Error))
	}
	return nil
}

func (m *MessengerPostgresRepository) ReadMessage(id string) (*domain.Message, error) {
	message := &domain.Message{}
	query := m.db.First(&message, "id = ? ", id)
	if query.RowsAffected == 0 {
		return nil, errors.New("message not found")
	}
	return message, nil
}

func (m *MessengerPostgresRepository) ReadMessages() ([]*domain.Message, error) {
	var messages []*domain.Message
	query := m.db.Find(&messages)
	if query.Error != nil {
		return nil, errors.New(fmt.Sprintf("messages not found: %v", query.Error))
	}
	return messages, nil
}